import numpy as np
from pathlib import Path
import pandas as pd
from datetime import datetime

def norm(x):
    normalized = (x - np.mean(x)) / np.std(x)
    median_normalized = np.median(normalized)
    return median_normalized

if __name__ == "__main__":
    # x = np.array([1, 2, 3, 4, 5, 6])
    # median = norm(x)
    # print(median)

    # data_path = Path("Data")
    # files = data_path.glob("**/heart_rate.csv")
    # for f in files:
    #     # zpracuj
    #     pass

    # sample_01 = data_path / Path("01/fitbit/heart_rate.csv")
    # s01 = pd.read_csv(sample_01)
    # print(s01["timesa"])


    data_path = Path("Data")
    average_heart_rates = {}

    for user_repo in data_path.iterdir():
        user_id = user_repo.name
        
        user_data = pd.DataFrame(columns=["value", "timestamp"])
        
        for file in user_repo.glob("**/heart_rate.csv"):
            sample = pd.read_csv(file)

            sample['timestamp'] = sample['date'].apply(lambda x: datetime.strptime(x.split(' GMT')[0], '%a %b %d %Y %H:%M:%S'))
            sample["hour"] = sample["timestamp"].dt.hour

            average_hr_per_hour = sample.groupby("hour")["value"].mean()
            average_heart_rates[user_id] = average_hr_per_hour

    for user_id, average_hr_per_hour in average_heart_rates.items():
        print(f"Uživatel {user_id}:")
        print(average_hr_per_hour)




# print(median_normalized)